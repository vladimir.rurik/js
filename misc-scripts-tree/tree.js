function tree(node, prefix = '', isLast = true) {
    // Main function logic
    console.log(prefix + (isLast ? '└── ' : '├── ') + node.name);
    // Check if there are nested items
    if (node.items) {
        // Process each nested item
        node.items.forEach((item, index, array) => {
            // Recursively call the function for each nested item
            tree(item, prefix + (isLast ? '    ' : '│   '), index === array.length - 1);
        });
    }
}

// Example input data
const data = {
    "name": 1,
    "items": [{
        "name": 2,
        "items": [{ "name": 3 }, { "name": 4 }]
    }, {
        "name": 5,
        "items": [{ "name": 6 }]
    }]
};

// Calling the function with the input data
tree(data);
