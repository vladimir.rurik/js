# Hierarchical Data Visualization Function

This repository contains the implementation of the `tree` function, a utility designed for visualizing hierarchical data structures in a structured tree format directly in the console.

## Overview

The `tree` function processes hierarchical objects that consist of `name` and optionally `items` properties to denote the structure and relationships within the data. It's designed to visually represent nested relationships, making it easier to understand complex hierarchies at a glance.

## Features

- **Recursive Visualization:** Handles deeply nested structures by recursively processing each level of the hierarchy.
- **Structured Output:** Prints the hierarchical data in a visually structured tree format, using indentation and branch symbols to denote parent-child relationships.
- **Console-based:** Designed to output directly to the console, making it suitable for debugging or presentation in terminal environments.

## Usage

To use the `tree` function, pass a hierarchical object to it as shown in the example below:

```javascript
const data = {
    "name": 1,
    "items": [{
        "name": 2,
        "items": [{ "name": 3 }, { "name": 4 }]
    }, {
        "name": 5,
        "items": [{ "name": 6 }]
    }]
};

tree(data);
```

This will output the following structured tree to the console:

```
1
└── 2
    ├── 3
    └── 4
└── 5
    └── 6
```

## Contributing

Contributions to improve the functionality or address issues with the `tree` function are welcome. Please feel free to submit a pull request or open an issue if you have suggestions or feedback.
