function deepEqual(actual, expected, path = '$') {
    if (actual === expected) {
        return true;
    }

    if (typeof actual !== 'object' || actual === null || typeof expected !== 'object' || expected === null) {
        throw new Error(`Mismatch at ${path}: expected ${expected} but got ${actual}`);
    }

    if (Array.isArray(actual) && Array.isArray(expected)) {
        if (actual.length !== expected.length) {
            throw new Error(`Array length mismatch at ${path}: expected length ${expected.length} but got ${actual.length}`);
        }
        for (let i = 0; i < actual.length; i++) {
            deepEqual(actual[i], expected[i], `${path}[${i}]`);
        }
    } else if (Array.isArray(actual) || Array.isArray(expected)) {
        throw new Error(`Type mismatch at ${path}: One is an array but the other is not`);
    } else {
        const actualKeys = Object.keys(actual);
        const expectedKeys = Object.keys(expected);
        if (actualKeys.length !== expectedKeys.length) {
            throw new Error(`Object key length mismatch at ${path}: expected keys ${expectedKeys.join(', ')} but got ${actualKeys.join(', ')}`);
        }
        for (const key of actualKeys) {
            if (!expected.hasOwnProperty(key)) {
                throw new Error(`Unexpected key ${key} at ${path}`);
            }
            deepEqual(actual[key], expected[key], `${path}.${key}`);
        }
    }
}

// Testing the function
const obj1 = {
  a: {
    b: 1,
  },
};
const obj2 = {
  a: {
    b: 2,
  },
};
const obj3 = {
  a: {
    b: 1,
  },
};

try {
    deepEqual(obj1, obj1); // OK
    console.log('OK');
} catch (error) {
    console.error(error.message);
}

try {
    deepEqual(obj1, obj2); // Error: $.a.b
    console.log('OK');
} catch (error) {
    console.error(error.message);
}

try {
    deepEqual(obj1, obj3); // OK
    console.log('OK');
} catch (error) {
    console.error(error.message);
}
