# Deep Equal Function

This JavaScript utility provides a robust implementation for deeply comparing two objects or arrays. It ensures that every property, including nested and array properties, are compared by value and type. Differences between the compared values are identified with precise error messages that indicate the exact path of the discrepancy.

## Features

- **Deep Comparison**: Recursively compares properties of objects and elements of arrays.
- **Error Reporting**: Provides detailed error messages using JSON Path notation to indicate where mismatches occur.
- **Type-Sensitive**: Ensures that the types of values are also compared, preventing false positives.

## Getting Started

To use this function, simply include the `deepEqual.js` script in your project and call the `deepEqual` function with two arguments that you want to compare.

### Installation

No installation is required. You can copy the function directly into your JavaScript codebase.

### Usage

Here is how you can use the `deepEqual` function:

```javascript
const obj1 = {
  a: {
    b: 1,
  },
};
const obj2 = {
  a: {
    b: 2,
  },
};
const obj3 = {
  a: {
    b: 1,
  },
};

try {
    deepEqual(obj1, obj1); // Should pass without errors
    console.log('Comparison is OK');
} catch (error) {
    console.error(error.message);
}

try {
    deepEqual(obj1, obj2); // Should throw an error indicating mismatch at $.a.b
    console.log('Comparison is OK');
} catch (error) {
    console.error(error.message);
}

try {
    deepEqual(obj1, obj3); // Should pass without errors
    console.log('Comparison is OK');
} catch (error) {
    console.error(error.message);
}
```

### Error Handling

When differences are found, the `deepEqual` function throws an error:

```javascript
try {
    deepEqual(object1, object2);
} catch (error) {
    console.error('Deep comparison error:', error.message);
}
```

## Contributing

Contributions to the `deepEqual` function are welcome. Whether it's refining the comparison algorithm, improving performance, or fixing bugs, please feel free to fork the repository, make changes, and submit a pull request.

## License

This project is open-sourced under the MIT license.

